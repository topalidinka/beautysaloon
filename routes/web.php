<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->prefix('admin')->name('admin.')->group(function() {
  Route::get('/settings', 'SettingController@showSettings')->name('settings');
  Route::post('/setwebhook', 'SettingController@setwebhook')->name('setwebhook');
  Route::post('/settings', 'SettingController@save_settings')->name('settings.store');
  Route::resource('/telusers', 'TeluserController');
  Route::resource('/merchants', 'MerchantController');
  Route::resource('/services', 'ServiceController');
  Route::resource('/portfolios', 'PortfolioController');
  Route::resource('/orders', 'OrderController');
  Route::post('/store', 'StoreController@store')->name('store');
  Route::get('/statistics', 'StatisticController@index')->name('statistics');
  Route::get('/push', 'PushController@index')->name('push');
  Route::post('/sendpush', 'PushController@send')->name('sendpush');
});

Route::post('746136767:AAEladwdXWukscggEwEFiVadBd_yICgdts0', function() {
  app('App\Http\Controllers\TelegramController')->webhookHandler();
})->name('bot-webhook');
