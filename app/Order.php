<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $fillable = ['date', 'description', 'service_id', 'teluser_id'];

  public function service() {
    return $this->belongsTo('App\Service');
  }

  public function teluser() {
    return $this->belongsTo('App\Teluser');
  }
}
