<?php

namespace App\Telegram;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use App\Http\Controllers\TelegramController;
use App\Http\Controllers\TeluserController;
use App\Setting;
use App\Telegram\TelegramHelper;

/**
 * Class StartCommand.
 */
class StartCommand extends Command
{

  /**
   * @var string Command Name
   */
  protected $name = 'start';

  /**
   * @var string Command Description
   */
  protected $description = 'Start command';
  
  /**
   * Handle
   */
  public function handle() {
  
    $this->replyWithChatAction([
      'action' => Actions::TYPING
    ]);

    $update   = $this->getUpdate();
    $name     = $update->getMessage()->getFrom()->getFirstName();
    $user     = TelegramController::getUserData($update->getMessage()->getFrom());
    $user     = TeluserController::setStep($user->id, 'start');
    $text     = TelegramHelper::getTextFromSettings('start_msg', $user);
    $response = $this->telegram->sendMessage([
      'chat_id' => $update->getMessage()->getFrom()->getId(),
      'text'    => $text
    ]);

    $telegramController = new TelegramController($this->telegram);
    $telegramController->start_menu($update->getMessage()->getFrom());
  }
}
