<?php

namespace App\Telegram;

use Telegram\Bot\Keyboard\Keyboard;
use App\Merchant;
use App\Setting;

class TelegramHelper {

  public static function generateServicesButtons($services, $callback_data) {

    $reply_markup = new Keyboard();
    $reply_markup->inline();
    foreach ($services as $service) {

      $service_btn = Keyboard::inlineButton([
        'text'          => $service['title'],
        'callback_data' => $callback_data . $service['id'],
      ]);
  
      $reply_markup->row(
        $service_btn
      );
    }
    return $reply_markup;
  }

  public static function getTextFromSettings($setting_key, $user) {

    $merch   = Merchant::firstOrFail();
    $from    = ['%first_name%', '%last_name%', '%address%', '%phone%'];
    $to      = [$user->first_name, $user->last_name	, $merch->address, $merch->number];
    $setting = Setting::where('field_name', '=', $setting_key)->first();
    $text    = str_replace($from, $to, $setting->value);
    
    return $text;
  }
}