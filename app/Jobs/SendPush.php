<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\TelegramController;

class SendPush implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  public $tries = 1;

  protected $user;

  protected $message;

  protected $image;

  protected $link;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($user, $message, $image, $link)
  {
    $this->user    = $user;
    $this->message = $message;
    $this->image   = $image;
    $this->link    = $link;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    app('App\Http\Controllers\TelegramController')->sendPush($this->user, $this->message, $this->image, $this->link);
  }
}
