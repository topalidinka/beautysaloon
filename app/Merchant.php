<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
  protected $fillable = ['title', 'address', 'number', 'description'];

  public function services() {
    $this->hasMany('App\Service');
  }
}
