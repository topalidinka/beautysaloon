<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
  
  protected $fillable = ['title', 'description', 'service_id', 'image'];

  public function service() {
    return $this->belongsTo('App\Service');
  }
}
