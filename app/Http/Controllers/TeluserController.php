<?php

namespace App\Http\Controllers;

use App\Teluser;
use Illuminate\Http\Request;

class TeluserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('admin.telusers.index', [
      'data' => Teluser::orderBy('created_at', 'desc')->get()
    ]);
  }

  public function edit($id)
  {
    $teluser = Teluser::findOrFail($id);
    return view('admin.telusers.edit', [
      'data' => $teluser,
    ]);
  }

  public function update(Request $request, $id)
  {
    $teluser = Teluser::findOrFail($id);
    $teluser->update($request->all());

    return redirect()->route('admin.telusers.index');
  }

  public function destroy($id)
  {
    Teluser::destroy($id);

    return redirect()->route('admin.telusers.index');
  }

  public static function setStep($id, $step, $detail = '')
  {
    $teluser = Teluser::findOrFail($id);
    $teluser->step   = $step;
    $teluser->detail = $detail;
    $teluser->save();

    return $teluser;
  }
}
