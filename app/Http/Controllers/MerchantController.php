<?php

namespace App\Http\Controllers;

use App\Merchant;
use Illuminate\Http\Request;

class MerchantController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('admin.merchants.index', [
      'data' => Merchant::orderBy('created_at', 'desc')->get()
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.merchants.create', [
      'data' => []
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $merchant = Merchant::create($request->all());

    return redirect()->route('admin.merchants.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $merchant = Merchant::findOrFail($id);
    return view('admin.merchants.edit', [
      'data' => $merchant
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $merchant = Merchant::findOrFail($id);
    $merchant->update($request->all());

    return redirect()->route('admin.merchants.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Merchant::destroy($id);

    return redirect()->route('admin.merchants.index');
  }
}
