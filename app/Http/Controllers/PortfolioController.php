<?php

namespace App\Http\Controllers;

use App\Portfolio;
use App\Service;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('admin.portfolios.index', [
      'data' => Portfolio::with('service', 'service.merchant')->orderBy('created_at', 'desc')->get()
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.portfolios.create', [
      'data'     => [],
      'services' => Service::get()
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $portfolio = Portfolio::create($request->all());

    return redirect()->route('admin.portfolios.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $portfolio = Portfolio::with('service', 'service.merchant')->findOrFail($id);
    return view('admin.portfolios.edit', [
      'data'     => $portfolio,
      'services' => Service::get()
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $portfolio = Portfolio::findOrFail($id);
    $portfolio->update($request->all());

    return redirect()->route('admin.portfolios.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Portfolio::destroy($id);

    return redirect()->route('admin.portfolios.index');
  }
}
