<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\SendPush;
use App\Teluser;

class PushController extends Controller
{
  public function index() {

    return view('admin.push', [
    ]);
  }

  public function send(Request $request) {

    $message = $request->get('content');
    $image   = $request->get('image');
    $link    = $request->get('link');

    $telusers = Teluser::get()->toArray();
    foreach($telusers as $teluser) {
      SendPush::dispatch($teluser, $message, $image, $link);
    }
    return redirect()->route('admin.telusers.index');
  }
}
