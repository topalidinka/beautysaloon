<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Api;
use Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use App\Teluser;
use App\Service;
use App\Order;
use App\Portfolio;
use App\Merchant;
use App\Setting;
use App\Http\Controllers\TeluserController;
use App\Telegram\TelegramHelper;
use Telegram\Bot\FileUpload\InputFile;

class TelegramController extends Controller
{

 /** @var Api */
 protected $telegram;

 public $methods = [
   'Записаться'        => 'service_sign',
   'Наши услуги'       => 'services_view',
   'Наши работы'       => 'portfolio_view',
   'Как добраться?'    => 'go_to_saloon',
   'Главное меню'      => 'start_menu',
   'Позвонить самому'  => 'call',
 ];

  /**
   * BotController constructor.
   *
   * @param Api $telegram
   */
  public function __construct(Api $telegram) {
    
    $this->telegram = $telegram;
  }

  /**
   * Get user data
   */
  public static function getUserData($user_data) {

    $user = Teluser::find($user_data->getId());
    if ( ! $user) {
      $user = Teluser::create(json_decode($user_data, true));
    }

    return $user;
  }
  
  /**
   * Handles incoming webhook updates from Telegram
   */
  public function webhookHandler() {

    $update   = $this->telegram->commandsHandler(true);
    $message  = $update->getMessage();
    $location = $message->getLocation();
    $contact  = $message->getContact();
    $from     = $message->getFrom();

    // Get location
    if ( ! empty($location)) {
      $this->generate_road($from, $location);
      $this->start_menu($from);
    }

    // Save phone
    if ( ! empty($contact)) {

      $userId      = $contact->getUserId();
      $phoneNumber = $contact->getPhoneNumber();
      $user        = Teluser::findOrFail($userId);
      $user->phone = $phoneNumber;
      $user->save();

      if ('service_buy' == $user->step) {
        $order    = Order::create($user->detail);
        $text     = TelegramHelper::getTextFromSettings('order_created', $user);
        $response = $this->telegram->sendMessage([
          'chat_id' => $userId,
          'text'    => $text
        ]);
        $user = TeluserController::setStep($user->id, 'service_paid');
        $this->start_menu($from);
      
        $admin = Teluser::where('is_admin', 1)->first();
        if (isset($admin->id)) {
          $text     = 'Создана новая заявка ' . "\xF0\x9F\x93\xA2";
          $view_order = 'https://botsalonkrasoty.ru/admin/orders/' . $order->id . '/edit';
          $reply_markup = new Keyboard();
          $reply_markup->inline();
          $link = Keyboard::inlineButton([
            'text' => 'Посмотреть',
            'url'  => $view_order,
          ]);
          $reply_markup->row($link);
          $response = $this->telegram->sendMessage([
            'chat_id'      => $admin->id,
            'text'         => $text,
            'reply_markup' => $reply_markup
          ]);
        }
      }
    }

    // Buttons handler
    $from = $message->getFrom();
    $text = $message->getText();
    if ( ! $from->is_bot && isset($this->methods[ $text ])) {
      $user = Teluser::findOrFail($from->getId());
    
      // Call method
      $is_exist = method_exists($this, $this->methods[ $text ]);
      if ($is_exist) {
        $this->{$this->methods[ $text ]}($from, $user);
      }
    }
   
    if ($update->getCallbackQuery()) {
      $query = $update->getCallbackQuery();
      $user  = Teluser::findOrFail($query->getFrom()->getId());
    
      // Call method
      $is_exist = method_exists($this, $query->getData());
      if ($is_exist) {
        $this->{$query->getData()}($query, $user);
      }

      // View service
      $is_view = strripos($query->getData(), 'service_view');
      if ($is_view > -1) {

        $service_id = str_replace('service_view', '', $query->getData());
        $this->service_view($query, $user, $service_id);
      }

      // Buy service
      $is_buy = strripos($query->getData(), 'service_buy');
      if ($is_buy > -1) {

        $service_id = str_replace('service_buy', '', $query->getData());
        $this->service_buy($query, $user, $service_id);
      }

      // Service portfolio 
      $is_portfolio = strripos($query->getData(), 'service_portfolio');
      if ($is_portfolio > -1) {

        $service_id = str_replace('service_portfolio', '', $query->getData());
        $this->service_portfolio($query, $user, $service_id);
      }
    }

   return 'Ok';
  }

  public function start_menu($from) {
   
    $user = Teluser::findOrFail($from->getId());
    $text = TelegramHelper::getTextFromSettings('select_action', $user);
 
    $service_sign = Keyboard::button([
      'text' => 'Записаться'
    ]);
    $services_view = Keyboard::button([
      'text' => 'Наши услуги'
    ]);
    $portfolio_view = Keyboard::button([
      'text' => 'Наши работы'
    ]);
    $go_to_saloon = Keyboard::button([
      'text' => 'Как добраться?'
    ]);

    $reply_markup = Keyboard::make([
      'keyboard'          => [[$service_sign, $services_view], [$portfolio_view, $go_to_saloon]],
      'resize_keyboard'   => true,
      'one_time_keyboard' => true
    ]);

    $response = $this->telegram->sendMessage([
      'chat_id'      => $from->getId(),
      'text'         => $text,
      'reply_markup' => $reply_markup
    ]);
  }

  public function services_view($from, $user) {

    $user     = TeluserController::setStep($user->id, 'services_view');
    $services = Service::get()->toArray();

    if ( ! empty($services)) {  
        
      $reply_markup = new Keyboard();
      $reply_markup->inline();
      $text = TelegramHelper::getTextFromSettings('services', $user);
      foreach ($services as $service) {

        $service_btn = Keyboard::inlineButton([
          'text'          => $service['title'],
          'callback_data' => 'service_view' . $service['id'],
        ]);
    
        $reply_markup->row(
          $service_btn
        );
      }

      $response = $this->telegram->sendMessage([
        'chat_id'      => $from->getId(),
        'text'         => $text,
        'reply_markup' => $reply_markup
      ]);
    } else {

      $text = 'Нет доступных услуг';
      $response = $this->telegram->sendMessage([
        'chat_id'      => $from->getId(),
        'text'         => $text
      ]);
    }
  }

  public function service_view($query, $user, $id) {

    $user    = TeluserController::setStep($user->id, 'service_view');
    $service = Service::find($id);
    
    $reply_markup = new Keyboard();
    $reply_markup->inline();

    if ($service) {

      $text  = 'Услуга "' . $service['title'] . '"'. PHP_EOL;
      $text .= 'Цена: ' . $service['price'] . ' руб' . PHP_EOL;
      $text .= 'Описание: ' . PHP_EOL . $service['description'] . PHP_EOL;
      
      $service_btn = Keyboard::inlineButton([
        'text'          => 'Записаться',
        'callback_data' => 'service_buy' . $service['id'],
      ]);
  
      $reply_markup->row($service_btn);
      $response = $this->telegram->sendMessage([
        'chat_id'      => $query->getFrom()->getId(),
        'text'         => $text,
        'reply_markup' => $reply_markup
      ]);
    } else {

      $text = 'Услуга не найдена';
      $response = $this->telegram->sendMessage([
        'chat_id' => $query->getFrom()->getId(),
        'text'    => $text
      ]);
    }
   
    $response = $this->telegram->answerCallbackQuery([
      'callback_query_id' => $query->getId()
    ]);
  }

  public function service_buy($query, $user, $id) {
    
    $date   = new \DateTime();
    $detail = [
      'teluser_id' => $user->id,
      'service_id' => $id,
      'date'       => $date
    ];
    $user = TeluserController::setStep($user->id, 'service_buy', $detail);
    $text = TelegramHelper::getTextFromSettings('send_phone', $user);
    
    $btn = Keyboard::button([
      'text'            => 'Отправить номер',
      'request_contact' => true
    ]);
    $btn2 = Keyboard::button([
      'text' => 'Главное меню'
    ]);
    $btn3 = Keyboard::button([
      'text' => 'Позвонить самому'
    ]);
    $keyboard = Keyboard::make([
      'keyboard'          => [[$btn], [$btn3], [$btn2]],
      'resize_keyboard'   => true,
      'one_time_keyboard' => true
    ]);

    $response = $this->telegram->sendMessage([
      'chat_id'      => $query->getFrom()->getId(),
      'text'         => $text,
      'reply_markup' => $keyboard
    ]);

    $response = $this->telegram->answerCallbackQuery([
      'callback_query_id' => $query->getId()
    ]);
  }

  public function service_portfolio($query, $user, $id) {
    
    $response = $this->telegram->answerCallbackQuery([
      'callback_query_id' => $query->getId()
    ]);

    $detail = [
      'teluser_id' => $user->id,
      'service_id' => $id,
    ];
    $user = TeluserController::setStep($user->id, 'service_portfolio', $detail);

    $portfolios = Portfolio::where('service_id', $id)->with('service')->get();
    if ($portfolios->count() > 0) {

      foreach ($portfolios as $portfolio) {

        $text  = $portfolio->title . PHP_EOL;
        if ($portfolio->description) {
          $text .= $portfolio->description . PHP_EOL;
        }
        $resource = $portfolio->image;
        
        $url   = 'https://' . $_SERVER['HTTP_HOST'] . $resource;
        $photo = InputFile::create($url);
       
        $response = $this->telegram->sendPhoto([
          'chat_id'      => $query->getFrom()->getId(),
          'photo'        => $photo,
          'caption'      => $text
        ]);
      }
   
      $reply_markup = new Keyboard();
      $reply_markup->inline();
      $service_btn = Keyboard::inlineButton([
        'text'          => 'Записаться',
        'callback_data' => 'service_buy' . $portfolio->service->id,
      ]);
  
      $reply_markup->row($service_btn);

      $text = 'Заинтересовались?';
      $response = $this->telegram->sendMessage([
        'chat_id'      => $query->getFrom()->getId(),
        'text'         => $text,
        'reply_markup' => $reply_markup
        
      ]);
    } else {

      $text = 'Нет доступных работ';
      $response = $this->telegram->sendMessage([
        'chat_id' => $query->getFrom()->getId(),
        'text'    => $text
      ]);
    }
  }

  public function service_sign($from, $user) {
    
    $user     = TeluserController::setStep($user->id, 'service_sign');
    $services = Service::get()->toArray();

    if ( ! empty($services)) {  
        
      $text = TelegramHelper::getTextFromSettings('services', $user);
      $reply_markup = TelegramHelper::generateServicesButtons($services, 'service_buy');
      $response = $this->telegram->sendMessage([
        'chat_id'      => $from->getId(),
        'text'         => $text,
        'reply_markup' => $reply_markup
      ]);
    } else {

      $text = 'Нет доступных услуг';
      $response = $this->telegram->sendMessage([
        'chat_id' => $from->getId(),
        'text'    => $text
      ]);
    }
  }

  public function portfolio_view($from, $user) {
    
    $user     = TeluserController::setStep($user->id, 'portfolio_view');
    $services = Service::get()->toArray();
    
    if ( ! empty($services)) {  
        
      $text = TelegramHelper::getTextFromSettings('portfolios', $user);
      $reply_markup = TelegramHelper::generateServicesButtons($services, 'service_portfolio');
      $response = $this->telegram->sendMessage([
        'chat_id'      => $from->getId(),
        'text'         => $text,
        'reply_markup' => $reply_markup
      ]);
    } else {

      $text = 'Нет доступных услуг';
      $response = $this->telegram->sendMessage([
        'chat_id' => $from->getId(),
        'text'    => $text
      ]);
    }
  }
  
  public function go_to_saloon($from, $user) {

    $user  = TeluserController::setStep($user->id, 'go_to_saloon');
    $text = TelegramHelper::getTextFromSettings('get_location', $user);

    $btn = Keyboard::button([
      'text'             => 'Отправить местоположение',
      'request_location' => true
    ]);
    $btn2 = Keyboard::button([
      'text' => 'Главное меню'
    ]);
    $btn3 = Keyboard::button([
      'text' => 'Позвонить самому'
    ]);
    $keyboard = Keyboard::make([
      'keyboard'          => [[$btn], [$btn3], [$btn2]],
      'resize_keyboard'   => true,
      'one_time_keyboard' => true
    ]);
  
    $response = $this->telegram->sendMessage([
      'chat_id'      => $from->getId(),
      'text'         => $text,
      'reply_markup' => $keyboard
    ]);
  }

  public function generate_road($from, $location) {

    $lat = $location->getLatitude();
    $lng = $location->getLongitude();

    $merch   = Merchant::firstOrFail();
    $address = urlencode($merch->address);
    $api = 'https://geocode-maps.yandex.ru/1.x?geocode=' . $address . '&results=1&format=json'; 
    $result = file_get_contents($api);

    $user = Teluser::findOrFail($from->getId());
    $text  = TelegramHelper::getTextFromSettings('send_location', $user);
    $text .= PHP_EOL . 'https://yandex.ru/maps/213/moscow/?ll=37.624780%2C55.753351&mode=routes&rtext=55.766202%2C37.605589~' . $lat . '%2C' . $lng . '&rtt=auto&z=14';
    $response = $this->telegram->sendMessage([
      'chat_id' => $from->getId(),
      'text'    => $text
    ]);
  }

  public function call($from) {

    $user = Teluser::findOrFail($from->getId());
    $text = TelegramHelper::getTextFromSettings('user_call', $user);
    $response = $this->telegram->sendMessage([
      'chat_id' => $from->getId(),
      'text'    => $text
    ]);
  }

  public function sendPush($user, $message, $image, $link) {

    // Text
    if ('' != $message && empty($image) && empty($link)) {
      $response = $this->telegram->sendMessage([
        'chat_id' => $user['id'],
        'text'    => $message
      ]);
    }

    // Text and Link
    if ('' != $message && empty($image) &&  ! empty($link)) {
      $reply_markup = new Keyboard();
      $reply_markup->inline();
      $btn = Keyboard::inlineButton([
        'text' => 'Открыть',
        'url'  => $link,
      ]);
      $reply_markup->row($btn);
      $response = $this->telegram->sendMessage([
        'chat_id'      => $user['id'],
        'text'         => $message,
        'reply_markup' => $reply_markup
      ]);
    }
   
    // Text and Image
    if ('' != $message &&  ! empty($image) && empty($link)) {

      $url   = 'https://botsalonkrasoty.ru'. $image;
      $photo = InputFile::create($url);
      $response = $this->telegram->sendPhoto([
        'chat_id' => $user['id'],
        'photo'   => $photo,
        'caption' => $message
      ]);
    }
   
    // Text and Image and Link
    if ('' != $message &&  ! empty($image) && ! empty($link)) {

      $url   = 'https://botsalonkrasoty.ru'. $image;
      $photo = InputFile::create($url);
      $reply_markup = new Keyboard();
      $reply_markup->inline();
      $btn = Keyboard::inlineButton([
        'text' => 'Открыть',
        'url'  => $link,
      ]);
      $reply_markup->row($btn);
      $response = $this->telegram->sendPhoto([
        'chat_id'      => $user['id'],
        'photo'        => $photo,
        'caption'      => $message,
        'reply_markup' => $reply_markup
      ]);
    }
  }
}
