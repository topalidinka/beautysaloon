<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StoreController extends Controller
{
  public function store(Request $request)
  {

    if ($request->hasFile('uploadFileObj')) {
      $file = $request->file('uploadFileObj');
    }

    $path = $file->store('public/img');
    $url  = \Storage::url($path);

    return response()->json([
      'url'     => $url,
      'success' => true
    ]);
  }
}