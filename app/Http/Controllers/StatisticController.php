<?php

namespace App\Http\Controllers;

use App\Teluser;
use App\Order;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
  public function index() {

    return view('admin.statistics', [
      'telusers' => Teluser::get(),
      'orders'   => Order::get()
    ]);
  }
}
