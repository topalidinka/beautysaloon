<?php

namespace App\Http\Controllers;

use App\Order;
use App\Teluser;
use App\Service;
use Illuminate\Http\Request;

class OrderController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('admin.orders.index', [
      'data' => Order::with('service', 'teluser')->orderBy('created_at', 'desc')->get()
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.orders.create', [
      'data'     => [],
      'services' => Service::get(),
      'telusers' => Teluser::get()
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $order = Order::create($request->all());

    return redirect()->route('admin.orders.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $order = Order::with('service', 'teluser')->findOrFail($id);
    return view('admin.orders.edit', [
      'data'     => $order,
      'services' => Service::get(),
      'telusers' => Teluser::get(),
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $order = Order::findOrFail($id);
    $order->update($request->all());

    return redirect()->route('admin.orders.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Order::destroy($id);

    return redirect()->route('admin.orders.index');
  }
}
