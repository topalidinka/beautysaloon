<?php

namespace App\Http\Controllers;

use App\Service;
use App\Merchant;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('admin.services.index', [
      'data' => Service::with('merchant')->orderBy('created_at', 'desc')->get()
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.services.create', [
      'data'      => [],
      'merchants' => Merchant::get()->toArray()
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $service = Service::create($request->all());

    return redirect()->route('admin.services.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $service = Service::findOrFail($id);
    return view('admin.services.edit', [
      'data'      => $service,
      'merchants' => Merchant::get()->toArray()
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $service = Service::findOrFail($id);
    $service->update($request->all());

    return redirect()->route('admin.services.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Service::destroy($id);

    return redirect()->route('admin.services.index');
  }
}
