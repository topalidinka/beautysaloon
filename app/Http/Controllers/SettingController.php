<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingController extends Controller
{
  
  public function showSettings() {

    $is_admin = (isset($_GET['is_admin'])) ? true : false;

    return view('admin.settings', [
      'settings' => Setting::get()->pluck('value', 'field_name'),
      'is_admin' => $is_admin
    ]);
  }

  public function setwebhook(Request $request) {

    $token = env('TELEGRAM_BOT_TOKEN');
    if ('' != $token) {

      $result = $this->sendTelegramData('setwebhook', [
        'query' => [
          'url' => $request->url . '/' . $token
        ]
      ]);

      return redirect()->route('admin.settings')->with('status', $result);
    }
  }

  public function save_settings(Request $request) {

    $settings = $request->except(['_token']);
    foreach($settings as $key => $val) {
      $setting = Setting::where('field_name', '=', $key)->first();
      if (empty($setting)) {
        Setting::create([
          'field_name' => $key,
          'value' => $val,
        ]);
      } else {
        $setting->value = $val;
        $setting->save();
      }
    }
    
    return redirect()->route('admin.settings');
  }


  public function sendTelegramData($route = '', $params = [], $method = 'post') {

    $token  = env('TELEGRAM_BOT_TOKEN');
    $client = new \GuzzleHttp\Client([
      'base_uri' => 'https://api.telegram.org/bot' . $token . '/'
    ]);
    $result = $client->request($method, $route, $params);

    return (string) $result->getBody();
  }
}
