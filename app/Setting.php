<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

  protected $fillable = [ 'field_name', 'value' ];

  public $timestamps = false;
}
