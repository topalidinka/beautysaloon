<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

  protected $fillable = ['title', 'price', 'description', 'merchant_id'];

  public function merchant() {
    return $this->belongsTo('App\Merchant');
  }

  public function portfolios() {
    $this->hasMany('App\Portfolio');
  }
}
