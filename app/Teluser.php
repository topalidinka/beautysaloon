<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teluser extends Model
{
  protected $fillable = ['id', 'is_bot', 'first_name', 'last_name', 'username', 'language_code', 'phone', 'step', 'detail', 'is_admin'];

  public function getDetailAttribute($value) {
    return unserialize($value);
  }

  public function setDetailAttribute($value) {
    $this->attributes['detail'] = serialize($value);
  }
}
