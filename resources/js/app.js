
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

window.Vue = require('vue')

import 'normalize.css'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'element-ui/lib/theme-chalk/index.css'


import Vuetify from 'vuetify'
import vUploader from 'v-uploader'
import VCharts from 'v-charts'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import axios from 'axios'
import VueAxios from 'vue-axios'
 
Vue.use(Vuetify)
Vue.use(vUploader)
Vue.use(VCharts)
Vue.use(ElementUI, { locale })
Vue.use(VueAxios, axios)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('telusers-table', require('./components/telusers/TelusersTable.vue'))
Vue.component('teluser-form', require('./components/telusers/TeluserForm.vue'))
Vue.component('merchants-table', require('./components/merchants/MerchantsTable.vue'))
Vue.component('services-table', require('./components/services/ServicesTable.vue'))
Vue.component('portfolios-table', require('./components/portfolios/PortfoliosTable.vue'))
Vue.component('orders-table', require('./components/orders/OrdersTable.vue'))

Vue.component('merchant-form', require('./components/merchants/Merchant.vue'))
Vue.component('service-form', require('./components/services/Service.vue'))
Vue.component('portfolio-form', require('./components/portfolios/Portfolio.vue'))
Vue.component('order-form', require('./components/orders/OrderForm.vue'))
Vue.component('settings-form', require('./components/Settings.vue'))
Vue.component('statistic-page', require('./components/StatisticPage.vue'))
Vue.component('push-notification-form', require('./components/PushNotificationForm.vue'))

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app'
});
