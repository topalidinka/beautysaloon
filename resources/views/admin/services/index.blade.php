@extends('layouts.app')

@section('content')
<div class="container">
  <services-table 
    :data="{{ json_encode($data) }}"
    :create="{{ json_encode(route('admin.services.create')) }}"
    :csrf_token="{{ json_encode(csrf_token()) }}" 
  ></services-table>
</div>
@endsection
