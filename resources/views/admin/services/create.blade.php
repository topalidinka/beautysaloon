@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <service-form 
        :service="{{ json_encode($data) }}"
        :merchants="{{ json_encode($merchants) }}"
        :csrf_token="{{ json_encode(csrf_token()) }}" 
        :action="{{ json_encode(route('admin.services.store'))}}"
      >
      </service-form>
    </div>
  </div>
</div>
@endsection
