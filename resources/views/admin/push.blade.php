@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <push-notification-form
        :action="{{ json_encode(route('admin.sendpush')) }}"
        :csrf_token="{{ json_encode(csrf_token()) }}" 
      ></push-notification-form>
  </div>
  </div>
</div>
@endsection
