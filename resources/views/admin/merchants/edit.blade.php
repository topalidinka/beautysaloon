@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <merchant-form
        :merchant="{{ json_encode($data) }}"
        :csrf_token="{{ json_encode(csrf_token()) }}"
      >
      </servmerchantice-form>
    </div>
  </div>
</div>
@endsection
