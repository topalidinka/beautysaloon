@extends('layouts.app')

@section('content')
<div class="container">
  <merchants-table 
    :data="{{ json_encode($data) }}"
    :create="{{ json_encode(route('admin.merchants.create')) }}"
    :csrf_token="{{ json_encode(csrf_token()) }}" 
  ></merchants-table>
</div>
@endsection
