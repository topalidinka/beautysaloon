@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <portfolio-form
        :portfolio="{{ json_encode($data) }}"
        :services="{{ json_encode($services) }}"
        :csrf_token="{{ json_encode(csrf_token()) }}"
      >
      </portfolio-form>
    </div>
  </div>
</div>
@endsection
