@extends('layouts.app')

@section('content')
<div class="container">
  <portfolios-table 
    :data="{{ json_encode($data) }}"
    :create="{{ json_encode(route('admin.portfolios.create')) }}"
    :csrf_token="{{ json_encode(csrf_token()) }}" 
  ></portfolios-table>
</div>
@endsection
