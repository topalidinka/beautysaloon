@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <order-form
        :order="{{ json_encode($data) }}"
        :services="{{ json_encode($services) }}"
        :telusers="{{ json_encode($telusers) }}"
        :csrf_token="{{ json_encode(csrf_token()) }}"
      >
      </order-form>
    </div>
  </div>
</div>
@endsection
