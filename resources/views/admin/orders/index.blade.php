@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <orders-table 
        :data="{{ json_encode($data) }}"
        :create="{{ json_encode(route('admin.orders.create')) }}"
        :csrf_token="{{ json_encode(csrf_token()) }}" 
      ></orders-table>
    </div>
  </div>
</div>
@endsection
