@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <settings-form
        :setwebhook="{{ json_encode(route('admin.setwebhook')) }}"
        :save="{{ json_encode(route('admin.settings.store')) }}"
        :csrf_token="{{ json_encode(csrf_token()) }}" 
        :is_admin="{{ json_encode($is_admin) }}"
        :settings="{{ json_encode($settings) }}"
      ></settings-form>
  </div>
  </div>
</div>
@endsection
