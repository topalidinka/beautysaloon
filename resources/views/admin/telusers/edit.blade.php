@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <teluser-form
        :teluser="{{ json_encode($data) }}"
        :csrf_token="{{ json_encode(csrf_token()) }}"
      >
      </teluser-form>
    </div>
  </div>
</div>
@endsection
