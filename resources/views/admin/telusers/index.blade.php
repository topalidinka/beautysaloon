@extends('layouts.app')

@section('content')
<div class="container">
  <telusers-table 
    :data="{{ json_encode($data) }}"
    :csrf_token="{{ json_encode(csrf_token()) }}" 
    :create="{{ json_encode(route('admin.push')) }}"
  ></telusers-table>
</div>
@endsection
