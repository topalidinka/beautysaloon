const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
   /*.webpackConfig({
      devServer: {
        proxy: {
          '/api/': {
           // target: 'https://geocode-maps.yandex.ru/1.x/?geocode=%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0,%20%D0%A2%D0%B2%D0%B5%D1%80%D1%81%D0%BA%D0%B0%D1%8F%20%D1%83%D0%BB%D0%B8%D1%86%D0%B0,%20%D0%B4%D0%BE%D0%BC%207&results=1&format=json',
            target: 'http://openlibrary.org/api/get?key=/b/OL1001932M', 
            //pathRewrite: {'^/api' : ''},
            secure: false,
            changeOrigin: true,
            logLevel: 'debug',
          },
        }
      },
    });
*/